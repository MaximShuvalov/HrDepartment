﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HRDepartment.Core.Services;
using HRDepartment.Model.DataBase;
using HRDepartment.Model.DTO;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace HRDepartment.ApiService.Controllers
{
    [EnableCors]
    [Route("api")]
    [ApiController]
    public class HrDepartmentController : ControllerBase
    {
        private readonly IDepartmentService _departmentService;
        private readonly IEmployeeService _employeeService;
        private readonly IMapper _mapper;

        public HrDepartmentController(IDepartmentService departmentService, IEmployeeService employeeService,
            IMapper mapper)
        {
            _departmentService = departmentService;
            _employeeService = employeeService;
            _mapper = mapper;
        }

        [HttpGet("ping")]
        public static OkResult Ping()
        {
            return new OkResult();
        }

        /// <summary>
        /// Получить список всех отделов
        /// </summary>
        /// <returns>Отделы предприятия</returns>
        [HttpGet("Department/All")]
        public async Task<List<Department>> Get()
        {
            return await _departmentService.GetAllDepartments();
        }

        /// <summary>
        /// Получить отдел
        /// </summary>
        /// <param name="key">Идентификатор отдела</param>
        /// <returns>Отдел</returns>
        [HttpGet("Department/{key}")]
        public async Task<DepartmentFullInfo> Get([FromRoute]long key)
        {
            return _mapper.Map<Department, DepartmentFullInfo>(await _departmentService.Get(key));
        }


        /// <summary>
        /// Уволенные сотрудники
        /// </summary>
        /// <param name="key">Идентификатор отдела</param>
        /// <returns>Список уволенных сотрудников</returns>
        [HttpGet("Firedemployees")]
        public async Task<List<FiredEmployee>> GetFiredEmployees(long key)
        {
            var department = await _departmentService.Get(key);
            return _mapper.Map<List<EmployeeLog>, List<FiredEmployee>>(department.EmployeeLogs
                .Where(p => p.Fired == true).ToList());
        }

        /// <summary>
        /// Трудоустроить сотрудника
        /// </summary>
        /// <param name="employee">Сотрудник</param>
        /// <param name="departmentKey">Идентификатор отдела</param>
        /// <param name="position">Позиция</param>
        [HttpPost("Recruit")]
        public async Task Recruit(Employee employee, long departmentKey, string position)
        {
            if (string.IsNullOrWhiteSpace(position)) throw new ArgumentException("Не указана должность");
            var department = await _departmentService.Get(departmentKey);
            if (department == null)
                throw new ApplicationException($"Отдела с идентификатором {departmentKey} не существует");
            await _departmentService.RecruitEmployee(employee, department, position);
        }

        /// <summary>
        /// Уволить сотрудника
        /// </summary>
        /// <param name="employeeKey">Идентификатор сотрудника</param>
        /// <param name="departmentKey">Идентификатор отдела</param>
        [HttpPost("Fire")]
        public async Task Fire(long employeeKey, long departmentKey)
        {
            await _departmentService.FireEmployee(employeeKey, departmentKey);
        }

        /// <summary>
        /// Создать отдел
        /// </summary>
        /// <param name="department">Отдел</param>
        [HttpPost("department")]
        public async Task<IActionResult> AddDepartment(Department department)
        {
            await _departmentService.Create(department);
            return Ok();
        }
    }
}